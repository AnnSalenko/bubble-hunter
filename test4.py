from tkinter import*
from random import*
from time import perf_counter
from math import *

HEIGHT = 500
WIDTH = 800


window=Tk()
window.title("Bubble Blaster")

c=Canvas(window,width=WIDTH, height=HEIGHT,bg='darkblue')
c.pack()

but_start=Button(window,text='Game Start', width=20, height=1,bg='green',fg='white')
but_start.pack(side=LEFT)


class Hunter():

     def __init__(self,x1=0,y1=0, size=30):
        self.x1=x1
        self.y1=y1
        self.x2=size
        self.y2=size
        self.r=size/2
        self.hunter0=c.create_polygon(5,5,5,25,30,15,fill='red')
        self.hunter1=c.create_oval(self.x1, self.y1, self.x2, self.y2,outline='red')
        c.move(self.hunter0,WIDTH/2,HEIGHT/2)
        c.move(self.hunter1,WIDTH/2,HEIGHT/2)

     def get_coords(self):
          pos=c.coords(self.hunter1)
          x=(pos[0]+pos[2])/2
          y=(pos[1]+pos[3])/2
          return [x,y]
                
h = Hunter()


    
def move_hunter(event):
        shift=10
        h0=h.hunter0
        h1=h.hunter1
        if event.keysym == 'Up':
            c.move(h0,0,-shift)
            c.move(h1,0,-shift)
        elif event.keysym == "Down":
            c.move(h0,0,shift)
            c.move(h1,0,shift)
        elif event.keysym == "Left":
            c.move(h0,-shift,0)
            c.move(h1,-shift,0)
        elif event.keysym == 'Right':
            c.move(h0,shift,0)
            c.move(h1,shift,0)

c.bind_all('<KeyPress>',move_hunter)

class Bubble:
    min_r = 10
    max_r = 30
    max_speed=20
    points=0

    def __init__(self,h=HEIGHT, w=WIDTH):
        self.speed = randint(1,Bubble.max_speed)
        self.x = randint(0,w)
        self.y = randint(0,h)
        self.r = randint(Bubble.min_r,Bubble.max_r)
        self.x1 = self.x - self.r
        self.y1 = self.y - self.r
        self.x2 = self.x + self.r
        self.y2 = self.y + self.r
        self.number = c.create_oval(self.x1,self.y1,self.x2,self.y2,outline="white")

    def move_bubble(self):
        speed=randint(1,self.max_speed)
        c.move(self.number,- speed,0)

    def get_coords(self):
        pos=c.coords(self.number)
        x=(pos[0]+pos[2])/2
        y=(pos[1]+pos[3])/2
        return [x,y]
       
    def distance(self,b):
        x1, y1 =self.get_coords()
        x2, y2 =b.get_coords()
        return sqrt((x2-x1)**2+(y2-y1)**2)

    def collision(self, b):
        points=0
        if self.distance(b) < (self.r+b.r):
            points += (self.r+self.speed)
        return points

c.create_text(50,30,text='TIME',fill='white')
c.create_text(150,30,text='SCORE',fill='white')
time_text=c.create_text(50,50,fill='white')
score_text=c.create_text(150,50,fill='white')
def show_score(score):
    c.itemconfig(score_text, text=str(score))
def show_time(time_left):
    c.itemconfig(time_text, text=str(time_left))

class Info:
    CHANCE=10
    TIME_LIMIT=30
    bubble=[]
    score=0

    def __init__(self):
        self.end = perf_counter()+Info.TIME_LIMIT
        self.bubble = Info.bubble
        self.score=Info.score
        

def main ():

    inf = Info()

    def repeated():
        if randint(1, inf.CHANCE) == 1:
            b=Bubble()
            inf.bubble.append(b)
        for i in range ((len(inf.bubble)-1), -1, -1):
            inf.bubble[i].move_bubble()
            score_help=inf.score
            inf.score += inf.bubble[i].collision(h)
            if inf.score != score_help:
                c.delete(inf.bubble[i].number)
                del inf.bubble[i]
        show_score(inf.score)
        show_time(int(inf.end-perf_counter()))
        window.update()
            
        if perf_counter()<inf.end:
            window.after(1,repeated)
        else:
            window.after_cancel(solve)
            for i in range (len(inf.bubble)):
                c.delete(inf.bubble[i].number)
            inf.bubble.clear()
            first_title=c.create_text(WIDTH/2, HEIGHT/2, text='GAME OVER', fill='white', font=('Helvetica', 30))
            second_title=c.create_text(WIDTH/2, HEIGHT/2 + 30, text='Score: ' + str(inf.score), fill='white')
            
        
        def start_fun(event):
            c.delete(first_title)
            c.delete(second_title)
            window.after(1,main)       
            
        but_start.bind("<Button-1>", start_fun)

    solve=window.after(0, repeated)
    
main ()
